# K.I. Bundesverband Regionalgruppen

Regionalgruppen des [Bundesverbands Künstliche Intelligenz e.V. (kurz: K.I. Verband)](https://ki-verband.de):

## Karte

### Bild/Druck

![map](Regionalgruppen.png)

Als [.png](Regionalgruppen.png) oder [.pdf](Regionalgruppen.pdf)

### HTML/Interaktiv

![Leaflet](leaflet.png)

siehe [regio.html](regio.html) zum Einbetten.

## Daten

Siehe regio.geojson (für z.B. QGIS) bzw. regio.js (für die Leaflet Karte)

Stand 05/2020, Pull Requests und Updates willkommen

`CC-BY2.0 Lizenz Paul Balzer, MechLab Engineering`